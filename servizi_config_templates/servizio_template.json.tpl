{
  "slug": "slug-servizio",
  "name": "Nome Servizio",
  "handler": "",
  "area": 32,
  "fcqn": "AppBundle\\Entity\\NomeEntita",
  "flow": "ocsdc.form.flow.scia_pratica_edilizia",
  "flowOperator": "ocsdc.form.flow.operatore.standardallegatonofirma",
  "description": "Descrizione servizio",
  "istruzioni": "Durante la compilazione della pratica ti sarà chiesto di inserire gli allegati relativi alla pratica e di effettuare un pagamento (tramite annullamento di una marca da bollo)",
  "url_modulo_principale": "https://s3.eu-west-3.amazonaws.com/ocsdc-moduli/1_Some_File.pdf",
  "url_moduli_aggiuntivi": [

  ],
  "paymentParameters": {
    "datiSpecificiRiscossione": "9/9999.9",
    "codIpaEnte": "C_H999",
    "password": "999999999999",
    "importo": "1200"
  },
  "paymentRequired": true,
  "status": 0
}

